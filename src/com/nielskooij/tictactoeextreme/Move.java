package com.nielskooij.tictactoeextreme;

import com.nielskooij.tictactoeextreme.board.Constants;

public class Move {
	
	private int microX;
	private int microY;
	
	public Move(int microX, int microY){
		this.microX = microX;
		this.microY = microY;
	}

	public int getMicroX() {
		return microX;
	}

	public void setMicroX(int microX) {
		this.microX = microX;
	}

	public int getMicroY() {
		return microY;
	}

	public void setMicroY(int microY) {
		this.microY = microY;
	}

	public int getMacroX() {
		return microX / Constants.MACRO_CELL_DIM;
	}

	public int getMacroY() {
		return microY / Constants.MACRO_CELL_DIM;
	}
	
	public int getNextMoveMacroX() {
		return microX % Constants.MACRO_CELL_DIM;
	}
	
	public int getNextMoveMacroY() {
		return microY % Constants.MACRO_CELL_DIM;
	}

	public Move deepCopy() {
		return new Move(microX, microY);
	}

	@Override
	public boolean equals(Object obj) {
		if(!(obj instanceof Move)) return false;
		Move o = (Move) obj;
		
		return o.microX == microX && o.microY == microX;
	}
	
	@Override
	public int hashCode() {
		return toString().hashCode();
	}
	
	@Override
	public String toString() {
		return String.format("place_move %s %s", microX, microY);
	}
	
}
