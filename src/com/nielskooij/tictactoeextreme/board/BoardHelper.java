package com.nielskooij.tictactoeextreme.board;

import java.util.ArrayList;
import java.util.List;

import com.nielskooij.tictactoeextreme.Move;

public class BoardHelper {

	/**
	 * List all the available moves on a board.
	 * @param board The board.
	 * @return The list of available moves.
	 */
	public static List<Move> getAvailableMoves(Board board){
		List<Move> availableMoves = new ArrayList<>();
		
		for(int x = 0; x < Constants.MICRO_CELL_DIM; x++) {
			for(int y = 0; y < Constants.MICRO_CELL_DIM; y++) {
				Move m = new Move(x, y);
				
				if(board.isValidMove(m)) {
					availableMoves.add(m);
				}
			}
		}
		
		return availableMoves;
	}
	
	/**
	 * Check if the game has ended.
	 * @param board The board.
	 * @return true if the game is over.
	 */
	public static boolean isGameOver(Board board) {
		return getWinner(board.getMacroBoard()) != Constants.EMPTY_CELL;		
	}
	
	/**
	 * Get the winner of the board.
	 * @param board The board.
	 * @return -1 if it is not over yet. Else the id of the winner will be returned.
	 */
	public static int getWinner(int[][] board) {
		//Check col	needs a lot of refactoring
		for(int y = 0; y < 3; y++){
			if(board[0][y] == board[1][y] && board[1][y] == board[2][y] && board[0][y] != -1 && board[0][y] != 0){
				return board[0][y];
			}
		}
				
		//Check row
		for(int x = 0; x < 3; x++){
			if(board[x][0] == board[x][1] && board[x][1] == board[x][2] && board[x][0] != -1 && board[x][0] != 0){
				return board[x][0];
			}
		}
				
		//Check diagonal
		if(board[0][0] == board[1][1] && board[1][1] == board[2][2] && board[0][0] != -1 && board[0][0] != 0){
			return board[0][0];
		}
		
		//Check anti-diagonal
		if(board[0][2] == board[1][1] && board[1][1] == board[2][0] && board[0][2] != -1 && board[0][2] != 0){
			return board[0][2];
		}
				
		//Check if full
		for(int y = 0; y < 3; y++){
			for(int x = 0; x < 3; x++){
				if(board[x][y] == 0 || board[x][y] == -1){
					return Constants.EMPTY_CELL;
				}
			}
		}
				
		return Constants.TIED_CELL;
	}
	
	/**
	 * Get a macro cell.
	 * @param x the x of the macrocell.
	 * @param y the y of the macrocell.
	 * @return a MACROCELL_DIM * MACROCELL_DIM array of the values in the specifix macro cell
	 */
	public static int[][] getMacroCellMatrix(Board board, int x, int y){
		int[][] macroCell = new int[Constants.MACRO_CELL_DIM][Constants.MACRO_CELL_DIM];
		
		for(int i = 0; i < Constants.MACRO_CELL_DIM; i++){
			for(int j = 0; j < Constants.MACRO_CELL_DIM; j++){
				
				macroCell[j][i] = board.getMicroBoard()[x * Constants.MACRO_CELL_DIM + j][y * Constants.MACRO_CELL_DIM + i];
				
			}
		}
		
		return macroCell;
	}
	
}
