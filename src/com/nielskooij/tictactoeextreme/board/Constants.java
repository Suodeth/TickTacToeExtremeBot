package com.nielskooij.tictactoeextreme.board;

public class Constants {

	public static final int MICRO_CELL_DIM = 9;
	public static final int MACRO_CELL_DIM = 3;
	
	/**
	 * Indicates a move can be done in this macrocell.
	 */
	public static final int PLACABLE_CELL = -1;
	
	/**
	 * Indicates the cell is empty. This can be both macro and microcells.
	 */
	public static final int EMPTY_CELL = 0;
	
	/**
	 * Indicates a macrocell tied.
	 */
	public static final int TIED_CELL = 3;
	
}
