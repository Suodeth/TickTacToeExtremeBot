package com.nielskooij.tictactoeextreme.board.score;

import com.nielskooij.tictactoeextreme.board.Board;

public interface IBoardScore {

	/**
	 * Calculate the score of a board for a specific player.
	 * @param board the board.
	 * @param player the player number.
	 * @param depth the depth of the minimax.
	 * @return the score.
	 */
	public int calculateScore(Board board, int player, int depth);
	
}
