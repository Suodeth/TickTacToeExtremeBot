package com.nielskooij.tictactoeextreme.board.score;

import com.nielskooij.tictactoeextreme.board.BoardHelper;
import com.nielskooij.tictactoeextreme.board.Constants;
import com.nielskooij.tictactoeextreme.board.Board;

public class SimpleScore implements IBoardScore {

	@Override
	public int calculateScore(Board board, int player, int depth) {
		int score = 0;
		
		int[][] macroCells = board.getMacroBoard();
		
		int whoWon = BoardHelper.getWinner(macroCells);
		if(whoWon != 0){
			if(whoWon == player){
				score += 100;
			}else if(whoWon != Constants.TIED_CELL){
				score -= 100;
			}
		}
		
		for (int x = 0; x < Constants.MACRO_CELL_DIM; x++) {
			for (int y = 0; y < Constants.MACRO_CELL_DIM; y++) {
				int cellValue = macroCells[x][y];
				
				if(cellValue == player){
					score += 10;
				}else if(cellValue != Constants.EMPTY_CELL && cellValue != Constants.TIED_CELL){
					score -= 10;
				}
			}
		}
		
		if(board.getTurn() == player){
			score += (depth * -1);
		}else{
			score += depth;
		}
		
		return score;
	}

}
