package com.nielskooij.tictactoeextreme.bot;

import java.util.List;
import java.util.Random;

import com.nielskooij.tictactoeextreme.Move;
import com.nielskooij.tictactoeextreme.board.Board;
import com.nielskooij.tictactoeextreme.board.BoardHelper;

public class BotRandom implements IBot {

	@Override
	public Move getBestMove(Board board) {
		List<Move> availableMoves = BoardHelper.getAvailableMoves(board);
		
		Random rand = new Random();
		int index = rand.nextInt(availableMoves.size());
		
		return availableMoves.get(index);
	}

}
