package com.nielskooij.tictactoeextreme.bot;

import com.nielskooij.tictactoeextreme.Move;
import com.nielskooij.tictactoeextreme.board.Board;

public interface IBot {

	/**
	 * Calculate the best move according to the bot.
	 * @param board the board.
	 * @return the best move according to the bot.
	 */
	public Move getBestMove(Board board);
	
}
