package com.nielskooij.tictactoeextreme.minimax;

import java.util.List;

import com.nielskooij.tictactoeextreme.board.BoardHelper;
import com.nielskooij.tictactoeextreme.Move;
import com.nielskooij.tictactoeextreme.board.Board;
import com.nielskooij.tictactoeextreme.board.score.IBoardScore;

public class Minimax {

	private int maxDepth;

	private IBoardScore scoring;
	private Move bestMove;

	public Minimax(IBoardScore scoring, int maxDepth) {
		this.scoring = scoring;
		this.maxDepth = maxDepth;
	}

	/**
	 * Calculate the best move. Use getBestMove() to retrieve the move.
	 * @param depth The current depth of the minimax.
	 * @return The score of the current calculation.
	 */
	public int calculateBestMove(int alpha, int beta, Board board, int depth) {
		if (depth >= maxDepth || BoardHelper.isGameOver(board)) {
			return scoring.calculateScore(board, board.getMyId(), depth);
		}

		depth++;

		List<Move> availableMoves = BoardHelper.getAvailableMoves(board);

		if (board.getTurn() == board.getMyId()) {
			return doMax(alpha, beta, board, depth, availableMoves);
		} else {
			return doMin(alpha, beta, board, depth, availableMoves);
		}
	}

	private int doMin(int alpha, int beta, Board board, int depth, List<Move> availableMoves) {
		int bestScore = Integer.MAX_VALUE;
		Move bestMoveMin = null;
		
		for (Move move : availableMoves) {

			board.doMove(move);
			int score = calculateBestMove(alpha, beta, board, depth);
			board.undoLastMove();

			if (score < bestScore) {
				bestScore = score;
				bestMoveMin = move;
			}

			if (score < beta) {
				alpha = score;
			}

			if (beta <= alpha) {
				bestMoveMin = move;
				break;
			}
		}

		bestMove = bestMoveMin;
		return bestScore;
	}

	private int doMax(int alpha, int beta, Board board, int depth, List<Move> availableMoves) {
		int bestScore = Integer.MIN_VALUE;
		Move bestMoveMax = null;

		for (Move move : availableMoves) {
			board.doMove(move);
			int score = calculateBestMove(alpha, beta, board, depth);
			board.undoLastMove();

			if (score > bestScore) {
				bestScore = score;
				bestMoveMax = move;
			}

			if (score >= alpha) {
				alpha = score;
			}

			if (alpha >= beta) {
				bestMoveMax = move;
				break;
			}
		}

		bestMove = bestMoveMax;
		return bestScore;
	}

	public Move getBestMove() {
		return bestMove;
	}

	public int getMaxDepth() {
		return maxDepth;
	}
	
}
