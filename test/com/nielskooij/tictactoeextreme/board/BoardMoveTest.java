package com.nielskooij.tictactoeextreme.board;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.nielskooij.tictactoeextreme.board.BoardParser;
import com.nielskooij.tictactoeextreme.Move;
import com.nielskooij.tictactoeextreme.board.Board;

public class BoardMoveTest extends BasicBoardTestSetup {

	@Test
	public void isValidMoveEmptyBoard(){
		board = new Board();
		Move m = new Move(2, 2);
		
		assertTrue(board.isValidMove(m));
	}

	@Test
	public void isValidMoveTakenCell(){
		Move m = new Move(5, 3);

		assertFalse(board.isValidMove(m));
	}
	
	@Test
	public void isValidMoveSpecialCase(){
		board.doMove(new Move(5, 5));
		
		assertFalse(board.isValidMove(new Move(6, 7)));
	}
	
	@Test
	public void doMoveSpecificMacroCellRight(){
		Move m = new Move(5, 5);
		
		doValidatedMove(m, true);
	}
	
	@Test
	public void doMoveSpecificMacroCellWrong(){
		Move m = new Move(2, 2);
		
		doValidatedMove(m, false);
	}
	
	@Test
	public void doMoveMacroCellFullRight(){
		board.doMove(new Move(5, 5));
		board.doMove(new Move(7, 7));
		
		Move m = new Move(2, 2);
		doValidatedMove(m, true);
	}
	
	@Test
	public void doMoveTakenMacroCell(){
		board.doMove(new Move(5, 5));
		board.doMove(new Move(7, 7));
		
		Move m = new Move(3, 3);
		doValidatedMove(m, false);
	}
	
	@Test
	public void updateMacroBoardTest() {
		board.doMove(new Move(5, 5));
		String expected = ".,.,.,.,1,.,.,.,-1";
	
		assertEquals(expected, BoardParser.macroBoardAsString(board));
	}
	
}
