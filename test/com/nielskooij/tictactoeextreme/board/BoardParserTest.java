package com.nielskooij.tictactoeextreme.board;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.nielskooij.tictactoeextreme.MatrixTestUtil;
import com.nielskooij.tictactoeextreme.board.BoardParser;

public class BoardParserTest extends BasicBoardTestSetup {

	@Test
	public void macroBoardTest() {
		String expected = ".,.,.,.,-1,.,.,.,.";
		
		assertEquals(expected, BoardParser.macroBoardAsString(board));
	}
	
	@Test
	public void microBoardTest() {
		String expected = "1,.,.,2,.,.,.,.,.,"
						+ ".,2,.,.,1,.,.,2,.,"
						+ ".,.,.,.,.,.,.,.,.,"
						
						+ ".,.,.,.,.,1,.,2,.,"
						+ ".,2,1,1,.,1,.,2,.,"
						+ ".,.,2,.,2,.,.,.,.,"
						
						+ ".,.,.,.,1,.,.,.,.,"
						+ ".,.,.,.,.,.,1,.,.,"
						+ ".,.,.,.,.,.,.,.,.";
		assertEquals(expected, BoardParser.microBoardAsString(board));
	}
	
	@Test
	public void macroFromStringTest() {
		String input = ".,.,.,.,-1,.,.,.,.";
		int[][] expected = MatrixTestUtil.buildMatrix(0, 0, 0, 0, -1, 0, 0, 0, 0);
		BoardParser.parseMacroBoard(input, board);
		
		MatrixTestUtil.intDoubleArrayEquals(expected, board.getMacroBoard(), Constants.MACRO_CELL_DIM, Constants.MACRO_CELL_DIM);
	}
	
	@Test
	public void microFromStringTest() {
		String input = "0,.,.,1,.,.,.,.,.,.,1,.,.,0,.,.,1,.,.,.,.,.,.,.,.,.,.,.,.,.,.,.,0,.,1,.,.,1,0,0,.,0,.,1,.,.,.,1,.,1,.,.,.,.,.,.,.,.,0,.,.,.,.,.,.,.,.,.,.,0,.,.,.,.,.,.,.,.,.,.,.";
		int[][] expected = MatrixTestUtil.buildMatrix(1,0,0,2,0,0,0,0,0,0,2,0,0,1,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,2,0,0,2,1,1,0,1,0,2,0,0,0,2,0,2,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0);
		BoardParser.parseMicroBoard(input, board);
		
		MatrixTestUtil.intDoubleArrayEquals(expected, board.getMicroBoard(), Constants.MICRO_CELL_DIM, Constants.MICRO_CELL_DIM);
	}
	
}
