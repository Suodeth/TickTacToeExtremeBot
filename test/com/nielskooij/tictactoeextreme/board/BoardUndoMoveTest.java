package com.nielskooij.tictactoeextreme.board;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.nielskooij.tictactoeextreme.Move;
import com.nielskooij.tictactoeextreme.board.BoardParser;

public class BoardUndoMoveTest extends BasicBoardTestSetup {
	
	@Test
	public void undoTestMicroBoard() {
		String expected = BoardParser.microBoardAsString(board);
		board.doMove(new Move(4, 4));
		board.undoLastMove();
		
		assertEquals(expected, BoardParser.microBoardAsString(board));
	}
	
	@Test
	public void undoTestMacroBoard() {
		String expected = BoardParser.macroBoardAsString(board);
		
		board.doMove(new Move(4, 3));
		board.undoLastMove();
		
		assertEquals(expected, BoardParser.macroBoardAsString(board));
	}
	
}
